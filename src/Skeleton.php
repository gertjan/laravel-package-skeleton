<?php

declare(strict_types=1);

namespace GertjanKrol\Skeleton;

final class Skeleton
{
    public const PACKAGE_VENDOR = 'gertjankrol';
    public const PACKAGE_NAME = 'skeleton';

    // Build your next great package.
}
